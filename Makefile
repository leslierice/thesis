ARTICLE  = article
all:
	pdflatex article
	bibtex article
	pdflatex article
	pdflatex article

clean:
	rm -R *~; rm *.bbl *.blg *.ps *.dvi *.aux *.out; rm *.idx; rm *.log; rm *.toc; rm */*~

