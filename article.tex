\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{lipsum}
\usepackage{listings}
\lstset{
  mathescape,
  basicstyle=\footnotesize,
  language=C,
  tabsize=2
  }

\usepackage{amsmath}
\usepackage[normalem]{ulem}
\usepackage{soul}
\usepackage{color}
\usepackage{colortbl}
\usepackage[margin=1.5in]{geometry}
\usepackage{geometry}
\newcommand{\figref}[1]{\figurename~\ref{#1}}

\title{Performance Optimization for the K-Nearest Neighbors Kernel using Strassen's Algorithm}
\author{Leslie Rice \\
Undergraduate Honors Thesis \\
\\
Committee: \\
Dr. Robert van de Geijn \\
Dr. Donald Fussell \\
Dr. Vijaya Ramachandran
}
\date{January 27, 2017}

\begin{document}

\maketitle

\begin{abstract}
Strassen's algorithm is a fast algorithm for matrix-matrix multiplication \cite{Strassen}. Recent advances have shown the benefit of using Strassen's algorithm to improve the performance of general matrix-matrix multiplication ({\tt{GEMM}}) for matrices of varying shapes and sizes \cite{StrassenReloaded}. These advances have created an opportunity to implement Strassen's algorithm in other matrix-matrix multiplication like operations. In this paper, we implement Strassen's algorithm in {\tt{GSKNN}} (General Stride $k$ Nearest Neighbors), a kernel for nearest neighbor search. The result is a performance speedup for large dimensions.
\end{abstract}

\section{Introduction}

Strassen's algorithm for matrix-matrix multiplication was introduced in 1969 \cite{Strassen}. Using Strassen's algorithm, $n \times n$ matrices can be multiplied in less than the conventionally required $O(n^3)$ arithmetic operations. Strassen's algorithm reduces the number of multiplications and increases the number of additions required in matrix-matrix multiplication. The use of Strassen's algorithm was not practical when implemented using the Basic Linear Algebra Subprograms (BLAS) {\tt{GEMM}} routine due to the additional memory movements and workspace required. However, a practical implementation of Strassen's algorithm was introduced when implemented in the BLAS-like Library Instantation Software (BLIS) \cite{BLIS} {\tt{GEMM}} routine \cite{StrassenReloaded}. The changes to data movement between memory layers in the BLIS approach to {\tt{GEMM}} reduce the negative impact of the extra arithmetic additions in Strassen's algorithm. This implementation of Strassen's algorithm outperforms conventional {\tt{GEMM}} for matrices of varying sizes and shapes and does not require additional workspace.

Nearest neighbor search is a problem of significant importance in computational geometry, non-parametric statistics, and machine learning. The common kernel ($k$NN kernel) in this problem involves searching for the $k$ nearest points of $m$ query points given $n$ reference points. On a high level, the $k$NN kernel involves computing a distance matrix $C\in\mathbb{R}^{\rm{m\times n}}$ between the $m$ query points and the $n$ reference points and selecting the neighbors. When the Euclidian distance metric is used for the distance calculation, the pairwise distance matrix $C$ can be computed using {\tt{GEMM}}. The BLIS approach to {\tt{GEMM}} has been used to improve upon the performance of the nearest neighbor problem in {\tt{GSKNN}} (General Stride $k$ Nearest Neighbors) \cite{GSKNN}. When $d$ is small, GSKNN can improve floating point operation efficiency by fusing distance calculation with the neighbor selection. However, the performance gain diminishes when in high dimension space. The structure of this kernel allows for a practical integration of Strassen's algorithm. We show that this integration grants speedup even in high dimensional space (large $d$).

Our contribution to this research is the porting of {\tt{GSKNN}} to the High Performance Machine Learning Primitives ({\tt{HMLP}}) framework, and the integration of Strassen's algorithm in {\tt{GSKNN}}. In this paper, we first discuss naive implementations of {\tt{GEMM}} and Strassen's algorithm, in order to then illustrate how Strassen's algorithm can be implemented for practical use. We give an overview of the nearest neighbor problem and the {\tt{GSKNN}} approach to solving the problem. Finally, we describe how Strassen's algorithm can be incorporated into {\tt{GSKNN}}. We explain the experimental setup, including the porting of {\tt{GSKNN}} to the {\tt{HMLP}} framework and the additional implementation of Strassen's algorithm. Finally, we give the experimental results, showing a performance speedup when using Strassen's algorithm for large dimensions.

There exist real-world examples in which it is necessary to solve the nearest neighbor problem with dense high dimensional data. The MNIST (Mixed National Institute of Standards and Technology) data set in 784 dimensions, for example, performs well with $k$NN classification. For sparse data that resides in higher than 10,000 dimensional space, there usually exists a dense low-rank feature space in a couple thousands dimensions. Thus, accelerating $k$NN with dense high dimensional data is important.

\section{General Matrix Multiplication}

We begin by describing the different approaches to general matrix-matrix multiplication. We first explain the naive implementation, then the use of submatrices, and finally the BLAS-like Library Instantiation Software (BLIS) approach of blocking and memory packing. When discussing {\tt{GEMM}}, we refer more specifically to the operation $ C := \alpha AB + \beta C $, where $ C $ is an $ m \times n $ matrix, A is an $ m \times k $ matrix, and B is a $ k \times n $ matrix.

\subsection{Naive Implementation}

The most basic approach to computing $ C := \alpha AB + \beta C $ involves three loops
(\figref{fig:triple}), which requires $ 2 m n k $ floating point operations (flops). \\

\begin{figure}[h]
\input triple_loop
\caption{Naive triple loop implementation for {\tt{GEMM}}.}
\label{fig:triple}
\end{figure}

\subsection{Computing with Submatrices}

Let’s assume $ m $, $ n $, and $ k $ are even, and the matrices $ A $, $ B $, and $ C $ have been partitioned as follows. \\

\[
C
=
\begin{bmatrix}
    C_{00} & C_{01} \\
    C_{10} & C_{11} \\
\end{bmatrix},
A
=
\begin{bmatrix}
    A_{00} & A_{01} \\
    A_{10} & A_{11} \\
\end{bmatrix},
B
=
\begin{bmatrix}
    B_{00} & B_{01} \\
    B_{10} & B_{11} \\
\end{bmatrix}
\] \\

A direct computation of $ C := AB + C $ using partitions is the following set of operations. Note that there are 8 multiplications and 8 additions.


\[
\begin{array}{l c l}
C_{00} &:=& A_{00} B_{00} + A_{01} B_{10} + C_{00} \\
C_{01} &:=& A_{00} B_{01} + A_{01} B_{11} + C_{01} \\
C_{10} &:=& A_{10} B_{00} + A_{11} B_{10} + C_{10} \\
C_{11} &:=& A_{10} B_{01} + A_{11} B_{11} + C_{11}
\end{array}
\]

\subsection{GotoBLAS algorithm for {\tt{GEMM}}  in BLIS}

The high performance implementation in the BLIS framework is derived from the GotoBLAS algorithm, and involves blocking $A$, $B$, and $C$ for three layers of cache, ordering the loops to take advantage of cache reuse, and packing data so that it will be accessed contiguously \cite{BLIS}. As shown in \figref{fig:blis}, the result is 6 layers of loops, with the outer 5 written in C and the inner-most loop, the micro-kernel, written in assembly. \\

\begin{figure}[h]
\input blis_origin
\caption{GotoBLAS algorithm for {\tt{GEMM}} in BLIS \cite{BLIS}.}
\label{fig:blis}
\end{figure}

\section{Strassen's Algorithm}

Strassen's algorithm is a matrix-matrix multiplication algorithm that reduces the number of multiplications in the operation $ C := \alpha AB + \beta C $ from 8 to 7 \cite{Strassen}. To demonstrate Strassen's algorithm, let's consider the matrices $ A $, $ B $, and $ C $ partitioned as described previously.

\subsection{Classic Strassen's Algorithm}

Using one level of Strassen's algorithm, we have the following set of operations, with 7 multiplications and 22 additions.

\begin{equation*}
\begin{array}{ l c l }
M_0 &:=& ( A_{00} + A_{11} ) ( B_{00} + B_{11} );  \\
M_1 &:=&  ( A_{10} + A_{11} ) B_{00};  \\
M_2 &:=&  A_{00} ( B_{01} - B_{11} ); \\
M_3 &:=&  A_{11}( B_{10} - B_{00} );  \\
M_4 &:=&  ( A_{00} + A_{01}) B_{11};   \\
M_5&:=&  (A_{10} - A_{00} )( B_{00} + B_{01} );  \\
M_6&:=&  (A_{01} - A_{11} )( B_{10} + B_{11} );  \\
C_{00} &:=& M_0 + M_3 – M_4 + M_7 + C_{00}; \\
C_{01} &:=& M_2 + M_4 + C_{01}; \\
C_{10} &:=& M_1 + M_3 + C_{10}; \\
C_{11} &:=& M_0 – M_1 + M_2 + M_5 + C_{11} \\
\end{array}
\label{eqn:stra}
\end{equation*}

By comparing direct computation and Strassen's algorithm, we see that the computational cost is $ 2 m n k $ flops versus $ (7/8)2 m n k $ flops. Strassen's algorithm can be performed as follows to give a general operation for one-level Strassen as outlined in \cite{StrassenReloaded}.

\begin{equation*}
\begin{array}{l @{\hspace{1pt}} c @{\hspace{1pt}} l l r}
M_0 &=& ( A_{00} + A_{11} ) ( B_{00} + B_{11} );
% \\ &
&
C_{00} +\!\!= M_0;  C_{11} +\!\!= M_0;  \\
M_1 &=&  ( A_{10} + A_{11} ) B_{00};
% \\ &
&
C_{10} +\!\!= M_1 ;  C_{11} -\!\!= M_1 ; \\
M_2 &=&  A_{00} ( B_{01} - B_{11} );
% \\ &
&
C_{01} +\!\!= M_2 ;  C_{11} +\!\!= M_2 ;
\\
M_3 &=&  A_{11}( B_{10} - B_{00} );
% \\ &
&
C_{00} +\!\!= M_3 ;  C_{10} +\!\!= M_3 ;
\\
M_4 &=&  ( A_{00} + A_{01}) B_{11};
% \\ &
&
C_{01} +\!\!=  M_4 ;   C_{00} -\!\!= M_4;
\\
M_5&=&  (A_{10} - A_{00} )( B_{00} + B_{01} );
% \\ &
&
C_{11} +\!\!= M_5;
\\
M_6&=&  (A_{01} - A_{11} )( B_{10} + B_{11} );
% \\ &
&
C_{00} +\!\!= M_6 ;
\end{array}
\label{eqn:allops}
\end{equation*} \\

Thus the general operation for one-level Strassen is the following:

\begin{equation}
M := (X + \delta Y)(V + \epsilon W);
C += \gamma_0 M; D += \gamma_1 M;
\label{e:genops}
\end{equation}

\subsection{A Practical Implementation}

Strassen's algorithm previously was considered to only be practical when multiplying very large and square matrices. However, Strassen's algorithm has been proven to be practical for small and rectangular matrices when integrated into the GotoBLAS algorithm for {\tt{GEMM}} in BLIS \cite{StrassenReloaded}.

\begin{figure}[h]
%\begin{center}
\input blis_strassen
%\end{center}
\caption{GotoBLAS algorithm for {\tt{GSKNN}}  in BLIS with Strassen.}
\label{fig:stra}
\end{figure}

\figref{fig:genops} provides a visualization of the general operation (\ref{e:genops}) using this algorithm. In this example, the Strassen implementation modifies the packing routines for $ A $ and $ B $, as well as the micro-kernel for updating an $ m_r \times n_r $ submatrix of $ C $. The incorporation of the additions of the submatrices of $A$ into $B$ into the packing routines reduces the negative impact of these additions. The micro-kernel is modified so that a small block of (X+Y)(V+W) is accumulated in $M_r$, which can then be added to the submatrices $C$ and $D$ by multiplying by $\alpha \gamma_0$ and $\alpha \gamma_1$ respectively.

\begin{figure}[h]
\begin{center}
\includegraphics[width=8cm]{Untitled.png}
\end{center}
\caption{Illustration representing the computation  of the general operation (\ref{e:genops}) (from \cite{StrassenReloaded} with the permission of the author).}
\label{fig:genops}
\end{figure}


\section{K-Nearest Neighbors Algorithm}

Nearest neighbor search is a problem of significant importance in computational geometry, non-parametric statistics, and machine learning. This problem involves searching for the $k$ nearest points of $m$ query points given $n$ reference points. Without approximation, the exhausted search in an arbitrary $d$ dimensional space requires $\mathcal{O}(mnk+mn\log(k))$ work. The $k$NN kernel searches neighbors (iteratively)\footnote{In high dimensional space, randomized tree or hashing methods will update the neighbor lists iteratively in each iteration with different space partitioning.} on a much smaller subset of $m$ queries and $n$ references. The performance of the $k$NN kernel is important, since it usually dominates the runtime and it is used in most of the large-scale neighbor search implementations. We first describe the problem in more detail, and then outline the method used in {\tt{GSKKN}}.

\subsection{Problem Definition}
In the nearest neighbors problem, we are given a set of $N$ reference points $\mathcal{X} := \{x_j\}_{j=1}^N$ and a query point $x$ in a $d$-dimensional space. The task is to find the set $\mathcal{N}_x$ of the $k$ nearest neighbors of $x$. Therefore, $\mathcal{N}_x$ is a set of $k$ points such that $\forall x_p \in \mathcal{N}_x$ we have $\|x-x_j\|_2 \geq \|x-x_p\|_2, \forall x_j \in \mathcal{X} \backslash \mathcal{N}_x$ \cite{GSKNN}. The distance computation between the $m$ query points and the $n$ reference points can be performed using the Euclidian distance metric. This is done by expansion as follows.
$$\|x_i-x_j\|_2^2 = \|x_i\|_2^2 + \|x_j\|_2^2 - 2x_i^\top x_j$$
The terms $x_i^\top x_j$ can be computed using a {\tt{GEMM}} routine.

\subsection{General Stride $k$ Nearest Neighbors}

{\tt{GSKNN}} is a $k$NN kernel that, rather than performing the distance calculation and then the neighbor selection in a sequential manner, fuses the two computations, resulting in a kernel that is over 4 times faster than existing methods \cite{GSKNN}. {\tt{GSKNN}} effectively embeds the various components of a $k$NN kernel inside the different levels of {\tt{GEMM}}, improving the cache performance by blocking and memory packing.

We describe {\tt{GSKNN}} in a $d$ dimensional space. We have a set of query points $Q$ and a set of reference points $R$, which are subsets of $\mathcal{X}$. We give pseudocode for {\tt{GSKNN}} in \figref{fig:gsknn} and \figref{fig:gsknnmicro} \cite{GSKNN}. {\tt{GSKNN}} partitions in the same manner as the GotoBLAS algorithm for GEMM in BLIS as described previously, containing six layers of loops. These six layers of loops partition in the $m$, $n$, and $d$ dimensions.

The 5th and outer loop partitions $R$ along the $n$ dimension with block size $n_c$. The 4th loop partitions the $d$ dimension with block size $d_c$. The 3rd loop partitions the $Q$ along the $m$ dimension with block size $m_c$. The 2rd and 1st loops, contained within the macro-kernel, continue to partition $Q$ and $R$ into smaller blocks. The 1st loop in the micro-kernel performs the distance calculation for a small $m_r \times n_r$ block of $C$. The neighbor selection follows this loop, using heap selection. The \textbf{if} statements ($p_c+d_c \ge d$) cause the heap selection and the square-norm accumulation only to occur in the last $p_c$ iteration. Like BLIS, the outer loops (1st-5th) are written in {\tt{C}} and the inner-most loop is written in assembly.

Also similar to BLIS, memory packing is used in {\tt{GSKNN}}, with the temporary buffers $Q^c(m_c,d_c)$, $R^c(d_c,n_c)$, $Q_2^c(m_c,1)$, $R_2^c(1,n_c)$, allowing for contiguous memory access in the macro- and micro-kernel. In the pseudocode in \figref{fig:gsknn}, we see that $Q^c$ and $R^c$ are packed directly from $\mathcal{X}$, and $Q_2^c$ and $R_2^c$ are packed directly from $\mathcal{X}_2$.

\begin{figure}[h]
\input gsknn
\caption{{\tt{GSKNN}} approach to $k$-Nearest Neighbors \cite{GSKNN}.}
\label{fig:gsknn}
\end{figure}

\begin{figure}[h]
\input gsknnmicro
\caption{{\tt{GSKNN}} Micro-kernel \cite{GSKNN}.}
\label{fig:gsknnmicro}
\end{figure}

\section{K-Nearest Neighbors with Strassen's Algorithm}

In this section, we describe how Strassen's algorithm can be integrated into  {\tt{GSKNN}}. As noted previously, the \textbf{if} statements ($p_c+d_c \ge d$) cause the heap selection and the square-norm accumulation only to occur in the last $p_c$ iteration. If we remove the \textbf{if} statements and everything contained within, the structure is very similar to the GotoBLAS algorithm for {\tt{GEMM}} in BLIS \figref{fig:blis}. In the case that $p_c$ is not in its last iteration, {\tt{GSKNN}} will only perform the rank-$d_c$ update \cite{GSKNN}:
\begin{equation}
    C += Q(p_c:p_c+d_c-1,:)^{\top}R(p_c:p_c+d_c-1,:)
\end{equation}
We accumulate this update result in the $C^c$ buffer. Because up until the last $p_c$ iteration {\tt{GSKNN}} will only perform the rank-$d_c$ update, we can use Strassen's algorithm in the iterations leading up to the last, in a similar manner as illustrated in \figref{fig:stra}. We show the pseudocode for the Strassen implementation in {\tt{GSKNN}} in \figref{fig:gsknnstra} and \figref{fig:gsknnmicrostra}. Here $V$ and $W$ are submatrices of $\mathcal{X}$, and $C$ and $D$ are submatrices of $C$. The additions of the submatrices \mathcal{V} and \mathcal{W} are incorporated into the packing of $R^c$ and $Q^c$.

\begin{figure}[h]
\input gsknn_stra
\caption{{\tt{GSKNN}} with Strassen.}
\label{fig:gsknnstra}
\end{figure}

\begin{figure}[h]
\input gsknnmicro_stra
\caption{{\tt{GSKNN}} Micro-kernel with Strassen.}
\label{fig:gsknnmicrostra}
\end{figure}

\section{Experimental Setup}

Before including an implementation of Strassen's algorithm in {\tt{GSKNN}}, we first ported {\tt{GSKNN}} to the High Performance Machine Learning Primitives {\tt{HMLP}} framework. {\tt{HMLP}} provides high performance, memory efficient matrix-matrix multiplication-like operations based on the BLIS framework. The purpose of {\tt{HMLP}} is to allow such code to be easily adopted into machine learning applications using operations such as kernel summation, nearest neighbor search, k-means clustering, convolution networks. The operations in {\tt{HMLP}} have C++ template frameworks and assembly kernels for different architectures\footnote{HMLP currently supports Intel x86-64, Knights Landning, Arm and NVIDIA GPUs.}. In order to add {\tt{GSKNN}} to {\tt{HMLP}}, we first ported the outer loops C code to C++ code matching the template style of already implemented operations, and included the double precision x86-64 micro-kernel designed for Intel Ivy-Bridge architectures.

We ran the experiments on the Maverick system at TACC, using blocking parameters with $m_r=8$, $n_r=4$, $k_c=256$, $m_c=104$, and $n_c=4096$. $m_r$ and $n_r$ are chosen based on the latency of the {\tt{VMUL}} and {\tt{VADD}} instructions, $d_c$ is chosen based on the L1 cache size, and $m_c$ and $n_c$ are chosen based on the L2 and L3 cache sizes \cite{GSKNN}. Multi-threaded experiments were performed using 10 cores, where the 3rd loop surrounding the micro-kernel is parallelized using OpenMP directives. We only perform experiments for values of $d>256$ because for smaller values of $d$, Strassen's algorithm is not applied.

\section{Results}

We show the performance results for {\tt{GSKNN}} with and without using Strassen's algorithm. We show the results of the single-core experiments in \figref{fig:perf1} and the multi-core experiments in \figref{fig:perf2}. We explore different sizes of $m$,$n$, and $k$, and plot the performance as we increase the dimension $d$. The performance metric we used is floating point efficiency:
\begin{equation}
  \text{\emph{effective} GFLOPS} = \frac{2\cdot m\cdot n\cdot k}{\text{time (in seconds)}}\cdot 10 ^{-9}.
  \label{e:egflops1}
\end{equation}
We see that with or without Strassen, performance increases with problem size $m$, $n$, and dimension $d$ but decreases with $k$. In the multi-core experiments especially, we see a larger difference in performance between the two implementations of {\tt{GSKNN}} is larger, with the Strassen implementation leading in performance. To summarize, in all of the single-core experiments, {\tt{GSKNN}} with Strassen outperforms {\tt{GSKNN}} without Strassen. In the multi-core experiments, for {\tt{GSKNN}} with Strassen outperforms {\tt{GSKNN}} without Strassen a majority of the time, when $m$, $n$, and $d$ are large enough.

\newgeometry{left=0.5in, right=0.5in}
\begin{figure*}[tb!]
\begin{center}
\includegraphics[width=5.5cm]{plot_st_2048_16.pdf}
\includegraphics[width=5.5cm]{plot_st_2048_128.pdf}
\includegraphics[width=5.5cm]{plot_st_2048_512.pdf}
\includegraphics[width=5.5cm]{plot_st_4096_16.pdf}
\includegraphics[width=5.5cm]{plot_st_4096_128.pdf}
\includegraphics[width=5.5cm]{plot_st_4096_512.pdf}
\includegraphics[width=5.5cm]{plot_st_8192_16.pdf}
\includegraphics[width=5.5cm]{plot_st_8192_128.pdf}
\includegraphics[width=5.5cm]{plot_st_8192_512.pdf}
\end{center}
\caption{Single-core performance of {\tt{GSKNN}} with Strassen (blue) vs. {\tt{GSKNN}} without Strassen (red) on an Intel\textsuperscript{\textregistered} Ivy-Bridge processor.}
\label{fig:perf1}
\end{figure*}

\begin{figure*}[tb!]
\begin{center}
\includegraphics[width=5.5cm]{plot_mt_2048_16.pdf}
\includegraphics[width=5.5cm]{plot_mt_2048_128.pdf}
\includegraphics[width=5.5cm]{plot_mt_2048_512.pdf}
\includegraphics[width=5.5cm]{plot_mt_4096_16.pdf}
\includegraphics[width=5.5cm]{plot_mt_4096_128.pdf}
\includegraphics[width=5.5cm]{plot_mt_4096_512.pdf}
\includegraphics[width=5.5cm]{plot_mt_8192_16.pdf}
\includegraphics[width=5.5cm]{plot_mt_8192_128.pdf}
\includegraphics[width=5.5cm]{plot_mt_8192_512.pdf}
\end{center}
\caption{10-core performance of {\tt{GSKNN}} with Strassen (blue) vs. {\tt{GSKNN}} without Strassen (red) on an Intel\textsuperscript{\textregistered} Ivy-Bridge processor.}
\label{fig:perf2}
\end{figure*}
\restoregeometry

\section{Conclusions}

Matrix multiplication is a very important operation in itself, and is a main component in many other operations in scientific applications. For example, machine learning is a growing field that necessitates computation with massive amounts of data, creating a need for operations that are as time-efficient as possible. Thus small improvements in such operations can result in significant gains. The BLIS framework contains a highly optimized method of multiplying matrices by means of blocking for cache sizes and packing for contiguous memory access. We described the benefits of implementing Strassen's algorithm in matrix-matrix multiplication like operations based on the BLIS framework. First, we observed one implementation version in the GotoBLAS algorithm for {\tt{GEMM}} in BLIS. Then, we described how such an implementation could be easily transferred to {\tt{GSKNN}}, another {\tt{GEMM}}-based operation. The performance speedup from combining {\tt{GSKNN}} with Strassen's algorithm is not only exciting because of the importance of efficiency in the nearest neighbor problem, which has extensive applications, but also because it demonstrates the likelihood of additional performance speedups when adding Strassen to other matrix-matrix multiplication like operations. We reiterate that the BLIS-like structure of blocking and memory packing in {\tt{GSKNN}} was essential to a practical implementation of Strassen's algorithm. Further research opportunities include implementing Strassen's algorithm into other matrix-matrix multiplication like operations based on the BLIS framework, specifically in other operations in the {\tt{HMLP}} framework.

\section{Acknowledgements}
I would like to thank Dr. Robert van de Geijn for his guidance during this research, and throughout my time in the SHPC group. I would also like to thank both Jianyu Huang and Chenhan Yu for their great support and help along the way. I thank my second reader Dr. Donald Fussell and committee member Dr. Vijaya Ramachandran for their input as well.

\bibliography{mybib}
\bibliographystyle{plain}

\end{document}
